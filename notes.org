#+TITLE: Notes sur comment afficher l'image de fiche perso

* Je peux remplacer  le contenu de "contourfeuille" par une iframe qui contient un lien vers la page retournée par mon serveur, qui affiche l'image
```HTML
  <iframe src="https://84.ip-51-38-239.eu:4732/player_image?user=laiaga" style="width: 100%; border: none;"></iframe>
```
* Sauf que passer le paramètre nom d'utilisateur est compliqué
** Je peux essayer de le passer avec "{postrow.displayed.POSTER_NAME}" sauf que ça génère du code HTML qui casse le JS
```HTML
<strong style="font-size:1.2em"><a href="/u1"><span style="color:#08BDFF"><strong>Ellen Nighteye</strong></span></a></strong>
```
** Sinon il faudrait que je trouve un moyen de récupérer l'id du posteur, "u1" etc, peut-être plus simple ?
** A toute fin utile : [[https://stackoverflow.com/questions/28295870/how-to-pass-parameters-through-iframe-from-parent-html]]
* Ajouter un deuxième div à l'intérieur de contourfeuille avec le texte et lui mettre un z-index supérieur
** [[https://stackoverflow.com/questions/4378361/placing-content-over-an-iframe]]
```HTML
<iframe src="http://phrogz.net/"></iframe>
<div id="over">HI MOM</div>
```
```CSS
#over { position:relative; top:20px; left:20px; z-index:2 }
```
** Il faudra voir comment régler la position relative pour que tout tienne dans la zone attendue
* En revanche -> pas trouvé comment faire pour garder le contour de "contourfeuille" et faire en sorte que l'iframe le remplisse sans déborder/agrandir ?
